# openmairie-devenv
openMairie tool to deploy a Debian Bullseye dev & tests environment for [openMairie Framework](http://www.openmairie.org/framework) based projects with Vagrant, VirtualBox and Ansible

## Requirements

- [Vagrant](https://www.vagrantup.com/) : last tested with 2.2.9
- [VirtualBox](https://www.virtualbox.org/) : last tested with 6.0.24
- [Ansible](https://www.ansible.com/) : last tested with 2.10.14

## Environnement

### Operating System

openmairie-devenv is deploying a [Debian](https://www.debian.org/) Bullseye dev & tests environnement with https://app.vagrantup.com/debian/boxes/bullseye64/. Last tested with : debian/bullseye64 (virtualbox, 11.20220912.1).

### Variables

You can run tests on any openMairie Framework based apps and any branch of this application. 

- **app_name**: `framework-openmairie` (default), `openelec`, `openaria`, ...
- **app_svn_branch_url**: `https://scm.adullact.net/anonscm/svn/openmairie/openmairie_exemple/trunk` (default), ...
- **php_version**: `7.4`, `8.0` (default), `8.1`
- **postgresql_version**: `10`, `11`, `12`, `13`, `14` (default)

## Deploy

    vagrant up

## Run tests

    vagrant ssh
    vagrant@bullseye:~# cd /openmairie/workspaces/omdevtools/
    vagrant@bullseye:~# . bin/activate
    (omdevtools) vagrant@bullseye:/openmairie/workspaces/omdevtools# cd /openmairie/workspaces/<app_name>/tests/
    (omdevtools) vagrant@bullseye:/openmairie/workspaces/<app_name>/tests# xvfb-run om-tests -c runall

>>>
During tests execution, watch these log files is important :

    vagrant ssh
    vagrant@bullseye:~$ sudo su -
    root@bullseye:~# tail -f /var/log/apache2/error.log /var/log/postgresql/postgresql-*-main.log /openmairie/workspaces/*/var/log/error.log
>>>
